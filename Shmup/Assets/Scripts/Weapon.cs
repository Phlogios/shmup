﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
	Cannon[] cannons;
	public string bulletLayer = "Bullets";

	// Use this for initialization
	void Start () {
		cannons = GetComponentsInChildren<Cannon> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TryFire(Vector3 inheritedVelocity, float holdTimer) {
		for (int i = 0; i < cannons.Length; i++) {
			cannons [i].TryFire (inheritedVelocity, holdTimer, bulletLayer);
		}
	}
}
