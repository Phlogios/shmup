﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class Path : MonoBehaviour {
	Waypoint[] _waypoints;

	void Awake () {
		_waypoints = GetComponentsInChildren<Waypoint> ();
	}
		
	void Start () {
		
	}

	void Update () {
		
	}

	void OnDrawGizmosSelected () {
		DrawLines ();
	}

	public void DrawLines() {
		_waypoints = GetComponentsInChildren<Waypoint> ();

		for (int i = 0; i < _waypoints.Length-1; i++) {
			Gizmos.color = Color.red;
			var wp1 = _waypoints [i].transform.position;
			var wp2 = _waypoints [i + 1].transform.position;
			Gizmos.DrawLine (wp1, wp2);

			Gizmos.color = Color.green;
			Gizmos.DrawLine (wp1, wp1 + _waypoints [i].transform.up);

			int steps = 10;
			for (int j = 0; j < steps; j++) {
				var pointOnLine = Vector3.Lerp (wp1, wp2, j / (float) steps);
				var interpolatedUp = Vector3.Slerp (_waypoints [i].transform.up, _waypoints [i + 1].transform.up, j / (float) steps);

				Gizmos.color = Color.gray;
				Gizmos.DrawLine(pointOnLine, pointOnLine + interpolatedUp * 0.5f);
			}
		}
	}

	public Waypoint GetWaypoint(int index) {
		if (index < _waypoints.Length) {
			return _waypoints [index];
		}
		return null;
	}
}
