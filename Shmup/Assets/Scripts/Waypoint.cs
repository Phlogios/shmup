﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {
	public bool isSnappedZ;
	public float targetVelocity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDrawGizmosSelected () {
		var path = GetComponentInParent<Path> ();
		path.DrawLines ();

		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere (transform.position, 0.5f);
	}
}
