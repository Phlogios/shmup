﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : MonoBehaviour {
	public float damage;
	public float exitVelocity;
	public float spin;
	bool hasCollided;

	Rigidbody _rigidbody;
	public Player owner;

	void Awake() {
		_rigidbody = GetComponent<Rigidbody> ();
	}

	void Start () {
	}

	void Update () {
		if (hasCollided) {
			gameObject.SetActive (false);
		}
		transform.Rotate (new Vector3 (0, 0, spin*Time.deltaTime));
	}

	void FixedUpdate () {
		var position = transform.position;

		if (position.x > 15.0f || position.x < -15.0f) {
			gameObject.SetActive (false);
		}
	}

	public void OnEnable() {
		hasCollided = false;
	}

	public void OnCollisionEnter(Collision collision) {
		hasCollided = true;
	}

	public Rigidbody GetRigidbody() {
		return _rigidbody;
	}
}
