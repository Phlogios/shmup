﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {
	public float maxHealth;
	float health;
	public UnityEvent healthChangedEvent = new UnityEvent();
	public UnityEvent deathEvent = new UnityEvent ();

	MeshRenderer meshRenderer;

	void Awake() {
		meshRenderer = GetComponent<MeshRenderer> ();
	}

	public void ResetHealth() {
		health = maxHealth;
		healthChangedEvent.Invoke ();
		if (meshRenderer) {
			meshRenderer.material.SetColor ("_EmissionColor", new Color (0, 0, 0));
		}
	}

	public void DoDamage(float damage) {
		health -= damage;
		healthChangedEvent.Invoke ();
		if (IsDead ()) {
			deathEvent.Invoke ();
		} else {
			if (meshRenderer) {
				StartCoroutine (DamageFade ());
			}
		}
	}

	public bool IsDead() {
		return health < 0.0f;
	}

	public float GetHealth() {
		return health;
	}

	public void OnEnable() {
		ResetHealth ();
	}

	public IEnumerator DamageFade() {
		for (float brightness = 1.0f; brightness >= 0; brightness -= 0.1f) {
			meshRenderer.material.SetColor ("_EmissionColor", new Color (brightness, brightness, brightness));
			yield return null;
		}
	}
}
