﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float maxSpeedX;
	public float maxSpeedY;
	public float acceleration;
	public float deceleration;

	public bool linearDeceleration;

	PlayerShip ship;
	Rigidbody _rigidbody;

	Vector2 inputDirection;
	Vector2 velocity;
	float angle;

	bool fireInput;
	float holdFireTimer = 0f;

	void Awake() {
		ship = GetComponentInChildren<PlayerShip> ();
		_rigidbody = ship.GetComponent<Rigidbody> ();
	}
		
	void Start () {
		
	}

	void Update () {
		inputDirection.x = Input.GetAxis ("Horizontal");
		inputDirection.y = Input.GetAxis ("Vertical");
		fireInput = Input.GetButton ("Fire1");
		if (!fireInput) {
			holdFireTimer = 0;
		} else {
			holdFireTimer += Time.deltaTime;
		}

		if (inputDirection.y != 0) {
			angle += inputDirection.y*Time.deltaTime*360.0f;
			angle = Mathf.Clamp (angle, -45.0f, 45.0f);
		} else {
			angle *= 0.9f;
		}
		ship.transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, angle));
	}

	void FixedUpdate() {
		Vector3 position = transform.position;

		velocity += inputDirection * acceleration;
		velocity.x = Mathf.Clamp (velocity.x, -maxSpeedX, maxSpeedX);
		velocity.y = Mathf.Clamp (velocity.y, -maxSpeedY, maxSpeedY);
		if (inputDirection.x == 0 && velocity.x != 0) {
			if (linearDeceleration) {
				float sign = Mathf.Sign (velocity.x);
				velocity.x -= deceleration * sign;
			} else {
				velocity.x *= deceleration;
			}
			if (Mathf.Abs(velocity.x) < 0.01) {
				velocity.x = 0;
			}
		}
		if (inputDirection.y == 0 && velocity.y != 0) {
			if (linearDeceleration) {
				float sign = Mathf.Sign (velocity.y);
				velocity.y -= deceleration * sign;
			} else {
				velocity.y *= deceleration;
			}
			if (Mathf.Abs(velocity.y) < 0.01) {
				velocity.y = 0;
			}
		}

		_rigidbody.velocity = velocity;

		if (fireInput) {
			ship.Fire (holdFireTimer);
		}
	}

	public Vector2 GetVelocity() {
		return velocity;
	}
}
