﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	public Text scoreLabel;
	public Text healthLabel;

	int score;

	void Start () {
		score = 0;
		var ship = GetComponentInChildren<PlayerShip> ();
		ship.health.healthChangedEvent.AddListener (UpdateHealth);
		UpdateScore ();
		UpdateHealth ();
	}

	void Update () {
		
	}

	public void SetScore(int score) {
		this.score = score;
		UpdateScore ();
	}

	public int GetScore() {
		return score;
	}

	public void UpdateScore() {
		scoreLabel.text = score.ToString ();
	}

	public void UpdateHealth() {
		var ship = GetComponentInChildren<PlayerShip> ();
		var health = ship.health.GetHealth();
		healthLabel.text = System.Math.Round(health, 2).ToString ();
		if (health < 0)
			healthLabel.text = "Dead";
	}
}
