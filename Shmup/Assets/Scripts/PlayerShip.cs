﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour {
	public Explosion explosion;

	public Health health;
	Rigidbody _rigidbody;

	Weapon[] weapons;

	void Awake() {
		_rigidbody = GetComponent<Rigidbody> ();
		health = GetComponent<Health> ();
		weapons = transform.GetComponentsInChildren<Weapon> ();
	}

	void Start () {
		health.ResetHealth ();
	}

	void Update () {
		var position = transform.position;

		if (position.y > 5.0f || position.y < -5.0f) {
			position.y = Mathf.Clamp (position.y, -5.0f, 5.0f);
			transform.position = position;
		}
		if (position.x > 8.0f || position.x < -8.0f) {
			position.x = Mathf.Clamp (position.x, -8.0f, 8.0f);
			transform.position = position;
		}
	}

	void FixedUpdate() {
		if (health.IsDead ()) {
			explosion.gameObject.SetActive (true);
			explosion.transform.position = transform.position;
			gameObject.SetActive (false);
		}
	}

	public void OnTriggerEnter(Collider collider) {
	}

	public void OnCollisionEnter(Collision collision) {
		var collider = collision.collider;

		var asteroid = collider.GetComponent<Asteroid> ();
		var enemy = collider.GetComponent<Enemy> ();
		if (asteroid || enemy) {
			health.DoDamage (collision.impulse.magnitude);
			var sparkObj = ObjectPool.sharedInstance.GetPooledObject ("Spark");
			sparkObj.transform.position = collision.contacts [0].point;
			sparkObj.SetActive (true);
		}

		Bolt bolt = collider.GetComponent<Bolt> ();
		if (bolt) {
			health.DoDamage (bolt.damage);
		}
	}

	public void OnEnable() {
		health.ResetHealth ();
	}

	public void Fire(float holdFireTimer) {
		for (int i = 0; i < weapons.Length; i++) {
			if(weapons[i].gameObject.activeInHierarchy)
				weapons [i].TryFire (_rigidbody.velocity, holdFireTimer);
		}
	}
}
