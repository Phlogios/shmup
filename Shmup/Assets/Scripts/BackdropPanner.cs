﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackdropPanner : MonoBehaviour {
	Material material;

	// Use this for initialization
	void Start () {
		var meshRenderer = GetComponent<MeshRenderer> ();
		material = meshRenderer.material;
	}
	
	// Update is called once per frame
	void Update () {
		var texOffset = material.mainTextureOffset;
		texOffset.y -= Time.deltaTime * 0.02f;
		material.mainTextureOffset = texOffset;
	}
}
