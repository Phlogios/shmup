﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	public float growSpeed;
	public float lifetime;

	AudioSource _audioSource;
	MeshRenderer _quad; 

	float lifetimeCounter;

	void Awake() {
		_quad = GetComponentInChildren<MeshRenderer> ();
		_audioSource = GetComponent<AudioSource> ();
	}
		
	void Start () {
		
	}

	void Update () {
		var scale = _quad.transform.localScale;
		scale += new Vector3 (1, 1, 0) * Time.deltaTime * growSpeed;
		_quad.transform.localScale = scale;

		var color = _quad.material.GetColor("_TintColor");
		color.a = lifetimeCounter / lifetime;
		color.a = Mathf.Clamp (color.a, 0, 1);
		_quad.material.SetColor("_TintColor", color);

		lifetimeCounter -= Time.deltaTime;

		if (lifetimeCounter < 0.0f) {
			_quad.gameObject.SetActive (false);
			if (!_audioSource.isPlaying) {
				gameObject.SetActive (false);
			}
		}
	}

	public void OnEnable () {
		_quad.gameObject.SetActive (true);
		_quad.transform.localScale = new Vector3 (1, 1, 1);
		_quad.material.SetColor("_TintColor", new Color(1, 1, 1, 1));
		lifetimeCounter = lifetime;
	}

	public static void Explode(GameObject owner, string explosionPrefabName) {
		owner.SetActive (false);
		var explosion = ObjectPool.sharedInstance.GetPooledObject (explosionPrefabName);
		explosion.transform.position = owner.transform.position;
		explosion.SetActive (true);
	}
}
