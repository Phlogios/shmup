﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnginePulseSine : MonoBehaviour {

	MeshRenderer _meshRenderer;

	void Awake () {
		_meshRenderer = GetComponentInChildren<MeshRenderer> ();
	}
		
	void Start () {
		
	}

	void Update () {
		Color color = _meshRenderer.material.GetColor("_TintColor");
		var normalizedSine = (Mathf.Sin (Time.time) + 1) * 0.5f;
		color.r = 0.25f;
		color.g = 0.25f*normalizedSine;
		color.b = 0.25f*normalizedSine;

		_meshRenderer.material.SetColor("_TintColor", color);
	}
}
