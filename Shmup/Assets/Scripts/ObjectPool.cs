﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem {
	public int poolCapacity;
	public GameObject objectPrefab;
	public bool dynamicArray;
}

public class ObjectPool : MonoBehaviour {
	public static ObjectPool sharedInstance;

	public List<ObjectPoolItem> itemsToPool;
	Dictionary<String, List<GameObject>> itemMap = new Dictionary<String, List<GameObject>> ();


	void Awake() {
		sharedInstance = this;
	}

	void Start () {
		foreach (ObjectPoolItem item in itemsToPool) {
			string objectName = item.objectPrefab.name;
			var objectList = new List<GameObject>();
			itemMap.Add (objectName, objectList);
			for (int i = 0; i < item.poolCapacity; i++) {
				GameObject obj = (GameObject)Instantiate(item.objectPrefab);
				obj.SetActive(false);
				objectList.Add(obj);
			}
		}
	}

	public GameObject GetPooledObject(string name) {
		var objectList = itemMap [name];
		for (int i = 0; i < objectList.Count; i++) {
			var pooledObject = objectList [i];
			if (!pooledObject.activeInHierarchy) {
				return pooledObject;
			}
		}
		foreach (ObjectPoolItem item in itemsToPool) {
			if (item.objectPrefab.name == name) {
				if (item.dynamicArray) {
					GameObject obj = (GameObject)Instantiate(item.objectPrefab);
					obj.SetActive(false);
					objectList.Add(obj);
					return obj;
				}
			}
		}
		return null;
	}
}