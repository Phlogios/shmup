﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public Vector3 velocity;
	public float maxSpeedY;
	public float acceleration;
	public Explosion explosionPrefab;

	Transform player;

	MeshRenderer shipMeshRenderer;
	Rigidbody _rigidbody;
	Health health;

	public Path path;
	int waypointCounter;
	Color originalColor;

	float holdFireTimer = 0f;
	Weapon[] weapons;

	void Awake() {
		player = FindObjectOfType<PlayerShip> ().transform;
		shipMeshRenderer = GetComponentInChildren<MeshRenderer> ();
		originalColor = shipMeshRenderer.material.color;
		_rigidbody = GetComponentInChildren<Rigidbody> ();
		health = GetComponentInChildren<Health> ();
		weapons = GetComponentsInChildren<Weapon> ();
		for (int i = 0; i < weapons.Length; i++) {
			weapons [i].bulletLayer = "EnemyBullets";
		}
	}
		
	void Start () {
	}
		
	void Update () {
		var position = transform.position;

		if (path) {
			var nextWaypoint = path.GetWaypoint(waypointCounter);
			if (nextWaypoint) {
				var intendedDirection = nextWaypoint.transform.position - position;
				
				var newDirection = Vector3.RotateTowards (transform.forward, intendedDirection, 2f * Time.deltaTime, 0.0f);
				var newRoll = Vector3.RotateTowards (transform.up, nextWaypoint.transform.up, 2f * Time.deltaTime, 0.0f);
				transform.rotation = Quaternion.LookRotation (newDirection, newRoll);
				_rigidbody.velocity = transform.forward * nextWaypoint.targetVelocity;
				
				if (intendedDirection.magnitude < 1.0f) {
					waypointCounter++;
				}
				
				if (nextWaypoint.isSnappedZ) {
					position.z = 0;
					transform.position = position;
				}
			} else {
				gameObject.SetActive (false);
			}
		}

		var color = shipMeshRenderer.material.color;
		if (Mathf.Abs (transform.position.z) > 0.5f) {
			color = Color.black;
		} else {
			color = originalColor;
		}
		shipMeshRenderer.material.color = color;

		var directionToPlayer = player.position - position;
		var angleToPlayer = Vector3.Angle (directionToPlayer, transform.forward);
		if (angleToPlayer < 30.0f && directionToPlayer.magnitude < 5.0f) {
			for (int i = 0; i < weapons.Length; i++) {
				weapons [i].TryFire (_rigidbody.velocity, holdFireTimer);
			}
			holdFireTimer += Time.deltaTime;
		} else {
			holdFireTimer = 0;
		}

		if (health.IsDead ()) {
			Explosion.Explode (gameObject, explosionPrefab.name);
		}
	}

	void FixedUpdate () {

	}

	public void OnEnable() {
		health.ResetHealth();
		velocity = new Vector2 (-5f, 0.0f);
		transform.localRotation = new Quaternion ();
		_rigidbody.angularVelocity = Vector3.zero;

		if (path) {
			var firstWaypoint = path.GetWaypoint (0);
			transform.position = firstWaypoint.transform.position;
			transform.rotation = firstWaypoint.transform.rotation;
		}

		waypointCounter = 0;
	}

	public void OnCollisionEnter(Collision collision) {
		var collider = collision.collider;

		Bolt bolt = collider.GetComponent<Bolt> ();
		if (bolt) {
			health.DoDamage (bolt.damage);
			bolt.owner.SetScore(bolt.owner.GetScore() + (int)bolt.damage*20);
			if (health.IsDead ()) {
				bolt.owner.SetScore (bolt.owner.GetScore () + 1000);
			}
		}
	}

	void OnDrawGizmosSelected() {
		//Draw line to next waypoint
		if (path) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine (transform.position, path.GetWaypoint (waypointCounter).transform.position);
		}
	}
}
