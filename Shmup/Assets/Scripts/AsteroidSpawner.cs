﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour {
	public static AsteroidSpawner sharedInstance;
	float timeUntilNextSpawn;

	public Transform[] asteroidPrefabs;
	List<string> asteroidNames = new List<string>();

	void Awake() {
		sharedInstance = this;
	}

	void Start () {
		timeUntilNextSpawn = 1.0f;

		for (int i = 0; i < asteroidPrefabs.Length; i++) {
			asteroidNames.Add (asteroidPrefabs [i].name);
		}
	}
	
	// Update is called once per frame
	void Update () {
		timeUntilNextSpawn -= Time.deltaTime;
		if (timeUntilNextSpawn < 0.0f) {
			var asteroid = SpawnAsteroid ();
			asteroid.transform.position = new Vector3 (20.0f, Random.Range (-5f, 5f), 0f);
			asteroid.GetComponent<Asteroid> ().size = (int)Random.Range (1, 4);
			Vector3 velocity = new Vector3();
			velocity.x = -Random.Range (1f, 3f);
			velocity.y = Random.Range (-0.05f, 0.05f);
			asteroid.GetComponent<Rigidbody> ().velocity = velocity;
			asteroid.SetActive (true);
			timeUntilNextSpawn = Random.Range (0, 10.0f);
		}
	}

	public GameObject SpawnAsteroid() {
		string asteroidName = asteroidNames [(int)Random.Range (0, asteroidNames.Count)];
		var asteroid = ObjectPool.sharedInstance.GetPooledObject (asteroidName);
		return asteroid;
	}

	void FixedUpdate() {
		
	}
}
