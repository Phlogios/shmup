﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {
	public float timeOffset;
	public float cooldown = 0.3f;
	public Bolt boltPrefab;
	AudioSource audioSource;
	float cooldownCounter = 0;
	Player owner;

	void Awake () {
		audioSource = GetComponent<AudioSource> ();
		owner = GetComponentInParent<Player> ();
	}

	void Update () {
		cooldownCounter -= Time.deltaTime;
		if (cooldownCounter < 0.0f) {
			cooldownCounter = 0;
		}
	}

	public void TryFire(Vector3 inheritedVelocity, float holdTimer, string bulletLayer) {
		if (cooldownCounter <= 0.0f && holdTimer > timeOffset) {
			Fire (inheritedVelocity, bulletLayer);
		}
	}

	void Fire(Vector3 inheritedVelocity, string bulletLayer) {
		cooldownCounter = cooldown;

		var bolt = ObjectPool.sharedInstance.GetPooledObject (boltPrefab.name);
		bolt.layer = LayerMask.NameToLayer(bulletLayer);
		bolt.transform.position = new Vector3 (transform.position.x, transform.position.y, 0f);

		var boltComponent = bolt.GetComponent<Bolt> ();
		boltComponent.GetRigidbody().velocity = transform.forward * boltComponent.exitVelocity + (inheritedVelocity * 0.2f);
		boltComponent.owner = owner;
		bolt.SetActive (true);
		audioSource.PlayOneShot (audioSource.clip);
	}
}
