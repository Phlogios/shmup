﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public Path[] paths;

	float timeUntilNextSpawn;
	float minTime = 3.0f;
	float maxTime = 6.0f;

	// Use this for initialization
	void Start () {
		timeUntilNextSpawn = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		timeUntilNextSpawn -= Time.deltaTime;
		if (timeUntilNextSpawn < 0.0f) {
			var enemyObj = ObjectPool.sharedInstance.GetPooledObject ("Enemy");
			var enemyComponent = enemyObj.GetComponent<Enemy> ();
			enemyComponent.path = paths [(int)Random.Range (0, paths.Length)];
			enemyObj.SetActive (true);
			timeUntilNextSpawn = Random.Range (minTime, maxTime);
		}

		minTime -= Time.deltaTime * 0.02f;
		minTime = Mathf.Max (0, minTime);

		maxTime -= Time.deltaTime * 0.02f;
		maxTime = Mathf.Max (3, maxTime);
	}
}
