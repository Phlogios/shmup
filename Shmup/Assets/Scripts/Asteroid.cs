﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
	public int size;

	Rigidbody _rigidbody;
	Health health;

	void Awake() {
		_rigidbody = GetComponent<Rigidbody> ();
		health = GetComponent<Health> ();
		health.deathEvent.AddListener (Explode);
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate () {
		if (transform.position.x < -15.0f || transform.position.x > 25.0f || transform.position.y < -15.0f || transform.position.y > 15.0f) {
			gameObject.SetActive (false);
		}
	}

	public void OnEnable() {
		transform.localScale = Vector3.one * Mathf.Pow(size, 1f/3f);
		health.maxHealth = size * size;
		health.ResetHealth ();

		RandomizeRotation ();
	}

	void RandomizeRotation() {
		var rotationAxis = Random.onUnitSphere;
		var angularVelocity = Random.Range (-2, 2);

		_rigidbody.angularVelocity = angularVelocity * rotationAxis;
	}

	public void OnCollisionEnter(Collision collision) {
		var collider = collision.collider;

		Bolt bolt = collider.GetComponent<Bolt> ();
		if (bolt) {
			health.DoDamage (bolt.damage);
		}
	}

	void Explode() {
		if (size > 1) {
			var randomVector = Random.onUnitSphere;
			for (int i = 0; i < size; i++) {
				var spawnedAsteroid = AsteroidSpawner.sharedInstance.SpawnAsteroid ();
				randomVector = Quaternion.Euler (0, 0, 360.0f / size) * randomVector;
				spawnedAsteroid.transform.position = transform.position + new Vector3 (randomVector.x, randomVector.y, 0.0f);
				spawnedAsteroid.GetComponent<Rigidbody> ().velocity = _rigidbody.velocity + (spawnedAsteroid.transform.position - transform.position).normalized*2f;
				spawnedAsteroid.GetComponent<Asteroid> ().size = size - 1;
				spawnedAsteroid.SetActive (true);
			}
		}
		var explosion = ObjectPool.sharedInstance.GetPooledObject ("AsteroidExplosion");
		explosion.transform.position = transform.position;
		explosion.SetActive (true);
		gameObject.SetActive (false);
	}

}
